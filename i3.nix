{ config, pkgs, callPackage, ... }:

{
  environment.pathsToLink = [ "/libexec" ];

  environment.variables.KDEWM = "/run/current-system/sw/bin/i3";
  services.xserver = {
    windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [
        dmenu
        i3blocks
      ];
    };
  };
}
