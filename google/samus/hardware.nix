self: super: {
  libevdevc = super.callPackage ./hardware/libevdevc.nix {};
  libgestures = super.callPackage ./hardware/libgestures.nix {};
  xf86-input-cmt = super.callPackage ./hardware/xf86-input-cmt.nix {};
  chromium-xorg-conf = super.callPackage ./hardware/chromium-xorg-conf.nix {};
}
