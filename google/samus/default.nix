{ config, lib, pkgs, ... }:

{

  imports = [
    ./modules
  ];

  boot.earlyVconsoleSetup = true;

  boot.supportedFilesystems = [ "f2fs" ];
  boot.plymouth.enable = true;
  boot.initrd.kernelModules = [
    "intel_agp"
    "i915"
  ];

  boot.extraModprobeConfig = ''
    options snd slots=snd_soc_sst_bdw_rt5677_mach,snd-hda-intel
  '';
  networking.hostName = "samus";
  networking.firewall.enable = true;
  networking.firewall.allowPing = true;

  services.xserver = {
    useGlamor = true;
    multitouch.enable = true;
    xkbModel = "chromebook";
    xkbOptions = "";

    dpi = 192;
    videoDrivers = [ "modesetting" ];
  };

  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs;
      [ vaapiIntel libvdpau libvdpau-va-gl vaapiVdpau ];

    extraPackages32 = with pkgs.pkgsi686Linux;
      [ vaapiIntel libvdpau libvdpau-va-gl vaapiVdpau ];
    s3tcSupport = true;
  };

  environment.variables = {
    VDPAU_DRIVER = "va_gl";
  };

  hardware.pulseaudio.enable = true;
  hardware.bluetooth.enable = true;

  hardware.enableAllFirmware = true;
  hardware.cpu.intel.updateMicrocode = true;
}
