{ stdenv, fetchFromGitHub, pkgconfig, xorgserver, xorgproto,
  utilmacros, libgestures, libevdevc }:

stdenv.mkDerivation rec {
  name = "xf86-input-cmt";
  src = fetchFromGitHub {
    owner = "GalliumOS";
    repo = "xf86-input-cmt";
    rev = "6537abb193ab59a59b95c9511fa1c94d942b2c11";
    sha256 = "15qb2fhg64pqsnm0mmw3vmv1zavdqfxymch7j5w7msyd0038m4lb";
  };

  builder = ./builder.sh;
  nativeBuildInputs = [ pkgconfig ];
  buildInputs = [
    xorgserver xorgproto utilmacros
    libgestures libevdevc
  ];

  hardeningDisable = [ "bindnow" "relro" ];
  configureFlags = [ "--with-sdkdir=${placeholder "out"}" ];
  meta = {
    description = "Chromebook touchpad driver";
    license = stdenv.lib.licenses.mit;
    platforms = stdenv.lib.platforms.linux;
  };
}
