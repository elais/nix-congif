{ stdenv, fetchFromGitHub, glib, jsoncpp, pkgconfig }:

stdenv.mkDerivation rec {
  name = "libgestures";
  src = fetchFromGitHub {
    owner = "GalliumOS";
    repo = "libgestures";
    rev = "7fa3186f3dc23f03998d7f2647812065e2e5c87e";
    sha256 = "0s3kphjd64zfnl9iqlzbbpdsdsilmz2d317sxic1crchla9rvwjj";
  };

  postPatch = ''
    substituteInPlace Makefile \
      --replace -Werror -Wno-error \
      --replace '$(DESTDIR)/usr/include' '$(DESTDIR)/include'
  '';

  nativeBuildInputs = [ pkgconfig ];
  buildInputs = [ glib jsoncpp ];

  makeFlags = [ "DESTDIR=$(out)" "LIBDIR=/lib" ];
}
