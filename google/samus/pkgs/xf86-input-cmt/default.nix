{ stdenv, fetchFromGitHub, pkgconfig, xorgserver, xorgproto,
  utilmacros, libgestures, libevdevc, xorg }:

stdenv.mkDerivation rec {
  name = "xf86-input-cmt";
  src = fetchFromGitHub {
    owner = "GalliumOS";
    repo = "xf86-input-cmt";
    rev = "6537abb193ab59a59b95c9511fa1c94d942b2c11";
    sha256 = "15qb2fhg64pqsnm0mmw3vmv1zavdqfxymch7j5w7msyd0038m4lb";
  };

  nativeBuildInputs = [ pkgconfig ];
  buildInputs = with xorg; [
    xorgserver xorgproto utilmacros
    libgestures libevdevc
  ];

  buildPhase = ''
    make INCLUDE="$NIX_CFLAGS_COMPILE -I${xorg.xorgserver.dev}/include/xorg -Iinclude"
  '';

  installPhase = ''
    make DESTDIR="$out" LIBDIR="lib" install
  '';

  hardeningDisable = [ "bindnow" "relro" ];
  configureFlags = [ "--with-sdkdir=${placeholder "out"}" ];

  meta = {
    description = "Chromebook touchpad driver";
    license = stdenv.lib.licenses.mit;
    platforms = stdenv.lib.platforms.linux;
  };
}
