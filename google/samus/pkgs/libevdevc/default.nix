{ stdenv, fetchFromGitHub, coreutils, pkgconfig, glib, jsoncpp }:

stdenv.mkDerivation rec {
  name = "libevdevc";
  src = fetchFromGitHub {
    owner = "GalliumOS";
    repo = "libevdevc";
    rev = "c13b51efca59e9e5831351c2c3605b40da1f4c45";
    sha256 = "0b0ysvix0kmqrppjm3ac1jj6lk0fg5q0r77qday2zzx30vz859wc";
  };

  postPatch = ''
    substituteInPlace common.mk \
      --replace /bin/echo ${coreutils}/bin/echo
    substituteInPlace include/module.mk \
      --replace /usr/include /include
  '';

  makeFlags = [ "DESTDIR=$(out)" "LIBDIR=/lib" ];
}
