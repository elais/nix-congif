{ config, pkgs, ... }:

let
  dotfiles = builtins.fetchGit {
    url = "https://gitlab.com/elais/dotfiles.git";
    rev = "a371df00d46b6348c809293936a308c2be2fe58d";
    ref = "master";
  };
in
{
  imports = [
  "${builtins.fetchTarball https://github.com/rycee/home-manager/archive/master.tar.gz}/nixos"
  ];
  home-manager.users.elais = {
    xdg.configFile."doom".source = "${dotfiles}/doom";
    programs.git = {
      package = pkgs.gitAndTools.gitFull;
      enable = true;
      userName = "Elais Player";
      userEmail = "elais@protonmail.com";
    };
    programs.emacs.enable = true;
    programs.home-manager.enable = true;
  };
}
