{ config, pkgs, ... }:

{
  imports = [
    ./google/samus/pkgs
  ];
  boot.earlyVconsoleSetup = true;

  boot.supportedFilesystems = [ "f2fs" ];
  boot.plymouth.enable = true;
  boot.initrd.kernelModules = [
    "intel_agp"
    "i915"
  ];

  networking.hostName = "samus";
  networking.firewall.enable = true;
  networking.firewall.allowPing = true;

  services.xserver = {
    # cmt.enable = true;
    useGlamor = true;
    xkbModel = "chromebook";
    xkbOptions = "";

    dpi = 192;
    videoDrivers = [ "modesetting" ];
    displayManager.sessionCommands = ''
      ${pkgs.xorg.xrdb}/bin/xrdb -merge <${pkgs.writeText "Xresources" ''
        Xft.dpi: 192
      ''}
    '';
  };

  users.extraUsers.root = {

    packages = with pkgs; [ f2fs-tools ];
    initialPassword = "nixos";
  };

  hardware.opengl = {
    extraPackages = with pkgs;
      [ vaapiIntel libvdpau libvdpau-va-gl vaapiVdpau ];

    extraPackages32 = with pkgs.pkgsi686Linux;
      [ vaapiIntel libvdpau libvdpau-va-gl vaapiVdpau ];

    s3tcSupport = true;
  };

  environment.variables = {
    VDPAU_DRIVER = "va_gl";
  };

  hardware.bluetooth.enable = true;

  hardware.enableAllFirmware = true;
  hardware.cpu.intel.updateMicrocode = true;
}

